# mysqlslap tests from VMs and k8s to dbod

Using the famous employees dataset.
https://dev.mysql.com/doc/employee/en/employees-installation.html
https://github.com/datacharmer/test_db

Table used for select queries:
```
MySQL [employees]> select count(*) from dept_emp;
+----------+
| count(*) |
+----------+
|   331603 |
+----------+
1 row in set (0.07 sec)

MySQL [employees]> select * from dept_emp LIMIT 1;
+--------+---------+------------+------------+
| emp_no | dept_no | from_date  | to_date    |
+--------+---------+------------+------------+
|  10001 | d005    | 1986-06-26 | 9999-01-01 |
+--------+---------+------------+------------+
```
Table used for single record select:
```
MySQL [employees]> select count(*) from employees limit 1;
+----------+
| count(*) |
+----------+
|   300024 |
+----------+
1 row in set (0.07 sec)

MySQL [employees]> select * from employees limit 1;
+--------+------------+------------+-----------+--------+------------+
| emp_no | birth_date | first_name | last_name | gender | hire_date  |
+--------+------------+------------+-----------+--------+------------+
|  10001 | 1953-09-02 | Georgi     | Facello   | M      | 1986-06-26 |
+--------+------------+------------+-----------+--------+------------+
1 row in set (0.00 sec)

MySQL [employees]> select * from employees where emp_no='40001';
+--------+------------+------------+-----------+--------+------------+
| emp_no | birth_date | first_name | last_name | gender | hire_date  |
+--------+------------+------------+-----------+--------+------------+
|  40001 | 1956-03-28 | Akemi      | Maliniak  | F      | 1987-08-06 |
+--------+------------+------------+-----------+--------+------------+
1 row in set (0.00 sec)
```

test1 (100 queries 1 client 1 time):
```
mysqlslap --user=*** --password="***" \
--host=dbod-***.cern.ch --port ***  \
--concurrency=1 --number-of-queries=100 \
--iterations=1 --create-schema=employees \
--query="SELECT * FROM dept_emp;" --verbose
```
test2 (1 query 1 client 100 times):
```
mysqlslap --user=*** --password="***" \
--host=dbod-***.cern.ch --port ***  \
--concurrency=1 --number-of-queries=1 \
--iterations=100 --create-schema=employees \
--query="SELECT * FROM dept_emp;" --verbose
```
test3 (1 query 1 client 100 times)
```
mysqlslap --user=*** --password="***" \
--host=dbod-***.cern.ch --port ***  \
--concurrency=1 --number-of-queries=1 \
--iterations=100 --create-schema=employees \
--query="select * from employees where emp_no='40001'" --verbose
```
* cc7 5.5.64-MariaDB gitlab-registry.cern.ch/strigazi/containers/mysqlslap:cc7
* f31 10.3.22-MariaDB gitlab-registry.cern.ch/strigazi/containers/mysqlslap:f31
* Debian 10 (buster) docker.io/library/mysql:5.7.29


| host-setup     | net    | image  | test1  | test2 | test3 |
| -              | -      | -      | -      | -     | -     |
| cc7 vm         | host   | -      | 19.836 | 0.190 | 0.001 |
| cc7-vm-dockerd | host   | cc7    | 19.205 | 0.190 |  |
| coreos-k8s     | host   | cc7    | 19.825 | 0.200 |  |
| coreos-k8s     | calico | cc7    | 19.063 | 0.199 | 0.004 |
| coreos-k8s     | host   | f31    | 19.865 | 0.203 |  |
| coreos-k8s     | calico | f31    | 19.523 | 0.204 |  |
| cc7-vm-dockerd | host   | f31    | 19.057 | 0.196 |  |
| coreos-k8s     | host   | debian | 36.845 | 0.502 |  |
| coreos-k8s     | calico | debian | 36.496 | 0.515 |  |
| cc7-vm-dockerd | host   | debian | 35.538 | 0.490 |  |


Interesting result, the debian based docker.io/library/mysql:5.7.29 is much slower
```
root@c2867d751255:/# cat /etc/os-release | grep "^NAME"
NAME="Debian GNU/Linux"
root@c2867d751255:/# mysqlslap --version
mysqlslap  Ver 1.0 Distrib 5.7.29, for Linux (x86_64)
root@c2867d751255:/# mysqlslap --user=*** --password="***" \
--host=dbod-***.cern.ch --port ***  \
--concurrency=1 --number-of-queries=100 \
--iterations=3 --create-schema=employees \
--query="SELECT * FROM dept_emp;" --verbose
Benchmark
	Average number of seconds to run all queries: 35.999 seconds
	Minimum number of seconds to run all queries: 35.999 seconds
	Maximum number of seconds to run all queries: 35.999 seconds
	Number of clients running queries: 1
	Average number of queries per client: 100
```
